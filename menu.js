var Menu = {
	preload(){
		var gameName = this.game.add.text(this.game.width/2, 150, 'Raiden', {
			font: '30px Arial',
			fill: '#ffffff'
		});
		gameName.anchor.setTo(0.5, 0.5);

		this.endless = this.game.add.text(this.game.width/2, 450, 'ENDLESS MODE', {
			font: '20px Arial',
			fill: '#ffffff'
		});
		this.endless.anchor.setTo(0.5, 0.5);
		this.startGame = this.game.add.text(this.game.width/2, 500, 'START', {
			font: '20px Arial',
			fill: '#ffffff'
		})
		this.startGame.anchor.setTo(0.5, 0.5);
		this.setting = this.game.add.text(this.game.width/2, 550, 'setting', {
			font: '20px Arial',
			fill: '#ffffff'
		})
		this.setting.anchor.setTo(0.5, 0.5);

		this.recEndless = this.game.add.graphics(0, 0);
		this.recEndless.lineStyle(2, 0x555555, 5);
		this.recEndless.drawRect(this.endless.x - 130, 450 - 25, 260, 45);

		this.recStart = this.game.add.graphics(0, 0);
		this.recStart.lineStyle(2, 0x555555, 5);
		this.recStart.drawRect(this.startGame.x - 130, 500 - 25, 260, 45);

		this.recSetting = this.game.add.graphics(0, 0);
		this.recSetting.lineStyle(2, 0x555555, 5);
		this.recSetting.drawRect(this.setting.x - 60, 550 - 20, 120, 35);
	},
	
	create(){
		this.cursor = this.game.input.keyboard.createCursorKeys();

		this.startGame.alpha = 1;
		this.pointer = this.game.input.mousePointer;
		this.game.camera.flash(0xffffff, 300);
	},
	
	update(){
		
		if(this.pointer.x >= this.startGame.x - 130
		&& this.pointer.x <=this.startGame.x +130
		&& this.pointer.y >= 500 - 25
		&& this.pointer.y <= 500 + 25)
		{
			this.startGame.alpha = 1;
			this.recStart.alpha = 1;

			this.setting.alpha = 0.8;
			this.recSetting.alpha = 0.5;

			this.endless.alpha = 0.8;
			this.recEndless.alpha = 0.5;

			if(this.pointer.leftButton.isDown){
				this.game.state.start('main');
				this.game.mode = 0;//normal
			}
		}
		else if(this.pointer.x >= this.setting.x - 60
			&& this.pointer.x <=this.setting.x +60
			&& this.pointer.y >= 550 - 20
			&& this.pointer.y <= 550 + 15)
		{
			this.startGame.alpha = 0.8;
			this.recStart.alpha = 0.5;

			this.setting.alpha = 1;
			this.recSetting.alpha = 1;

			this.endless.alpha = 0.8;
			this.recEndless.alpha = 0.5;

			if(this.pointer.leftButton.isDown){
				this.game.state.start('setting');
			}
		}
		else if(this.pointer.x >= this.endless.x - 130
			&& this.pointer.x <=this.endless.x +130
			&& this.pointer.y >= 450 - 20
			&& this.pointer.y <= 450 + 15){
			this.startGame.alpha = 0.8;
			this.recStart.alpha = 0.5;
	
			this.endless.alpha = 1;
			this.recEndless.alpha = 1;

			this.setting.alpha = 0.8;
			this.recSetting.alpha = 0.5;
	
			if(this.pointer.leftButton.isDown){
				this.game.state.start('main');
				this.game.mode = 1;//endless
			}
		}
		else{
			this.startGame.alpha = 0.8;
			this.recStart.alpha = 0.5;
			this.setting.alpha = 0.8;
			this.recSetting.alpha = 0.5;
			this.endless.alpha = 0.8;
			this.recEndless.alpha = 0.5;
		}
	}
}
