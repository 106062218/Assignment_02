var Setting = {
	preload(){
		this.game.load.image('volume', './images/volume.jpg');
	},

	create(){
		this.volume = this.game.add.text(300, 200, 'VOLUME', {
			font: '20px Arial',
			fill: '#ffffff'
		});
		this.volume.anchor.setTo(0.5, 0.5);

		this.volumeBarRec = this.game.add.graphics(0, 0);
		this.volumeBarRec.lineStyle(2, 0x555555, 5);
		this.volumeBarRec.drawRect(this.game.width/2 - 150, 230, 300, 30);
		this.volumeBarRec.anchor.setTo(0.5, 0.5);
		this.volumeBar = this.game.add.sprite(this.game.width/2 - 150, 230,'volume');

		this.back = this.game.add.text(this.game.width/2, 550, 'back', {
			font: '20px Arial',
			fill: '#ffffff'
		})
		this.back.anchor.setTo(0.5, 0.5);

		this.recBack = this.game.add.graphics(0, 0);
		this.recBack.lineStyle(2, 0x555555, 5);
		this.recBack.drawRect(this.back.x - 60, 530, 120, 35);

		this.cursor = this.game.input.keyboard.createCursorKeys();
		this.pointer = this.game.input.mousePointer;
	},

	update(){
		this.volumeBar.width = this.game.sound.volume * 300;

		if(this.pointer.x >= this.game.width/2 - 60
			&& this.pointer.x <= this.game.width/2 + 60
			&& this.pointer.y >= 530
			&& this.pointer.y <= 565)
			{
				this.back.alpha = 1;
				this.recBack.alpha = 1;
	
				if(this.pointer.leftButton.isDown){
					this.game.state.start('menu');
				}
			}
		else {
			this.back.alpha = 0.8;
			this.recBack.alpha = 0.5;
		}

		if(this.pointer.x >= this.game.width/2 - 150
			&& this.pointer.x <= this.game.width/2 + 150
			&& this.pointer.y >= 230
			&& this.pointer.y <= 260){
			if(this.pointer.leftButton.isDown){
				this.volumeBar.width = this.pointer.x - (this.game.width/2 - 150);
				this.game.sound.volume = this.volumeBar.width / 300;
			}
		}
	}
}