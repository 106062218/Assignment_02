# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Website Detail Description

# Basic Components Description : 
How to play: 遊戲中只需使用滑鼠。主角飛機會跟隨滑鼠游標, 且會自動發射子彈。

1. Jucify mechanisms : 
	a. 	主畫面中可選擇兩種模式, 普通以及 endless 模式。其中普通模式有三波的敵人, 打完三波敵人則過關。		Enless 模式會有無限多波敵人, 在玩家陣亡後結束。
	b.	加速: 左下角的藍色方框體積會隨著時間自動增加, 只要其體積不為零, 常按滑鼠左鍵則可加快飛機飛行速度。
		攻擊加速: 右下角的橘色方框的體積會在你殺死一個敵人後增加。當橘色方框集滿後, 快速點擊兩下滑鼠左鍵, 便可使飛機射擊速度加快。
	c. 暫停: 在遊戲開始後, 按下左下角的 'PAUSE' 字樣, 可暫停遊戲。 在暫停遊戲階段, 可以調整音量, 回主畫面, 或回到遊戲。
2. Animations :
	a. 主角飛機: 仔細看飛機尾端, 它的火焰尾巴會動喔! 啾咪
	b. 敵人: 揮動翅膀, 以及死亡動畫。
	c. 敵人火焰子彈: 恩, 這也有動畫。
	d. 閃爍的補包
3. Particle Systems : 
	當主角飛機射出的子彈擊中敵人, 或者主角被敵人的火焰子彈打中時。
4. Sound effects : 
	a. 當主角被敵人擊中, 以及主角補血時, 有不同的音效。
	b. 音量: 在主畫面 -> setting 裡, 以及遊戲暫停時可調整音量大小。
5. Leaderboard : 
	在 Endless mode 結束後, 會顯示 database 前三名的分數, 以及玩家的分數。

# Bonus Functions Description : 
1. 在每個的人被殺死後, 會有一定擊率掉下閃爍的愛心圖示, 能使角色回血。
