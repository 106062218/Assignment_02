var Main = {
	preload(){
		this.game.load.spritesheet('airplane', './images/airplane.png', 64, 64);
		this.game.load.image('pixel', './images/pixel.png');
		this.game.load.spritesheet('fire-bullet', './images/fire-bullet.png', 64, 64);
		this.game.load.image('yellow', './images/yellow.jpg');
		this.game.load.image('bullet', './images/bullet.png');
		this.game.load.spritesheet('enemy', './images/monster.png', 64, 64);
		this.game.load.image('bg', './images/background.gif', 640, 960);
		this.game.load.image('red', './images/red.jpg', 600, 30);
		this.game.load.image('blue', './images/blue.jpg', 50, 90);
		this.game.load.image('orange', './images/orange.jpg', 50, 90);
		this.game.load.spritesheet('health', './images/health.png', 125, 125);
		this.game.load.audio('heal', './audio/heal.mp3');
		this.game.load.audio('crash', './audio/crash.mp3');
		this.game.load.image('volume', './images/volume.jpg');
	},

	create(){
		this.a = this.game.sound.volume;

		this.stage = 1;
		this.bg = this.game.add.tileSprite(0, 0, 800, 700, 'bg');
		this.bg.scale.setTo(2.5, 1.5);
		this.game.physics.startSystem(Phaser.Physics.ARCADE);
		this.game.renderer.renderSession.roundPixels = true;

		this.hpBar = this.game.add.graphics(0, 0);
		this.hpBar.lineStyle(2, 0x555555, 5);
		this.hpBar.drawRect(this.game.width/2 - 300, 660, 600, 30);
		this.hpBar.anchor.setTo(0.5, 0.5);
		this.healthbar = this.game.add.sprite(this.game.width/2 - 300, 660,'red');

		this.turbo = this.game.add.graphics(0, 0);
		this.turbo.lineStyle(2, 0x555555, 5);
		this.turbo.drawRect(25, 600, 50, 90);
		this.turbo.anchor.setTo(0.5, 0.5);
		this.turbobar = this.game.add.sprite(25, 690,'blue');
		this.turbobar.height = 0;
		this.turboing = false;

		this.machineGun = this.game.add.graphics(0, 0);
		this.machineGun.lineStyle(2, 0x555555, 5);
		this.machineGun.drawRect(725, 600, 50, 90);
		this.machineGun.anchor.setTo(0.5, 0.5);
		this.machinegunbar = this.game.add.sprite(725, 690,'orange');
		this.machinegunbar.height = 0;

		if(this.game.mode === 1) this.scoretxt = this.game.add.text(10, 10, 'Score: 0', {
			font: '20px Arial',
			fill: '#ffffff'
		});
		this.score = 0;
		this.game.score = 0;
		
		this.pointer = this.game.input.mousePointer;


		this.airplane = this.game.add.sprite(this.game.width/2, 450, 'airplane');
		this.airplane.anchor.setTo(0.5, 0.5);
		this.airplane.animations.add('straight', [2, 0], 30,  true);
		this.airplane.animations.add('left', [12, 14], 30,  true);
		this.airplane.animations.add('right', [5, 3], 30,  true);
		this.game.physics.arcade.enable(this.airplane);
		this.airplane.animations.play('straight');

		this.healSound = this.game.add.audio('heal');
		this.crashSound = this.game.add.audio('crash');		

		this.enemies = this.game.add.group();
		this.enemies.enableBody = true;
		this.enemies.createMultiple(10, 'enemy');
		this.enemies.hitPoint = 10;

		this.bullets = this.game.add.group();
		this.bullets.enableBody = true;

		this.fires = this.game.add.group();
		this.fires.enableBody = true;

		this.heals = this.game.add.group();
		this.heals.enableBody = true;

		this.game.time.events.loop(1500, this.createEnemy, this);
		if(this.game.mode === 0) this.game.time.events.loop(1500, this.createEnemy2, this);
		if(this.game.mode === 0) this.game.time.events.loop(1500, this.createEnemy3, this);
		this.airplaneFireLoop = this.game.time.events.loop(400, this.createFire, this);
		
		this.emitter = this.game.add.emitter(0, 0, 15);
        this.emitter.makeParticles('pixel');
		this.emitter.setScale(2, 0, 2, 0, 999);
		this.emitter.gravity = 0;

		this.bulletExplode = this.game.add.emitter(0, 0, 5);
		this.bulletExplode.makeParticles('yellow');
		this.bulletExplode.setScale(2, 0, 2, 0, 800);
		this.bulletExplode.gravity = 0;

		this.explode = this.game.add.emitter(0, 0, 30);
		this.explode.makeParticles('pixel');
		this.explode.setScale(3, 0, 3, 0, 3000);
		this.explode.gravity = 0;

		this.airplaneHealth = 100;
		this.game.camera.flash(0xffffff, 300);

		this.game.gameovertxt = 'GAME OVER!!';


		this.pause_label = this.game.add.text(100, 620, 'PAUSE', {
			font: '20px Arial',
			fill: '#ffffff'
		});
		this.pause_label.inputEnabled = true;
		this.pause_label.events.onInputUp.add(() => {
			this.game.paused = this.game.paused? false : true;
			this.pause_label.text = this.game.paused? 'RESUME' : 'PAUSE';
			if(this.game.paused){
				this.volume = this.game.add.text(300, 200, 'VOLUME', {
					font: '20px Arial',
					fill: '#ffffff'
				});
				this.volume.anchor.setTo(0.5, 0.5);
		
				this.volumeBarRec = this.game.add.graphics(0, 0);
				this.volumeBarRec.lineStyle(2, 0x555555, 5);
				this.volumeBarRec.drawRect(this.game.width/2 - 150, 230, 300, 30);
				this.volumeBarRec.anchor.setTo(0.5, 0.5);
				this.volumeBar = this.game.add.sprite(this.game.width/2 - 150, 230,'volume');


				this.back = this.game.add.text(this.game.width/2, 550, 'menu', {
					font: '20px Arial',
					fill: '#ffffff'
				})
				this.back.anchor.setTo(0.5, 0.5);
		
				this.recBack = this.game.add.graphics(0, 0);
				this.recBack.lineStyle(2, 0x555555, 5);
				this.recBack.drawRect(this.back.x - 60, 530, 120, 35);
				this.game.time.events.loop(10, this.pauseUpdate, this);
			}
			else {
				this.back.destroy();
				this.recBack.destroy();
				this.volume.destroy();
				this.volumeBar.destroy();
				this.volumeBarRec.destroy();
			}
		});
	},

	pauseUpdate(){
		this.volumeBar.width = this.game.sound.volume * 300;
		if(this.pointer.x >= this.game.width/2 - 150
			&& this.pointer.x <= this.game.width/2 + 150
			&& this.pointer.y >= 230
			&& this.pointer.y <= 260){
			if(this.pointer.leftButton.isDown){
				this.volumeBar.width = this.pointer.x - (this.game.width/2 - 150);
				this.game.sound.volume = this.volumeBar.width / 300;
				this.a = this.volumeBar.width / 300;
			}
		}

		if(this.pointer.x >= this.game.width/2 - 60
			&& this.pointer.x <= this.game.width/2 + 60
			&& this.pointer.y >= 530
			&& this.pointer.y <= 565)
			{
				this.back.alpha = 1;
				this.recBack.alpha = 1;
	
				if(this.pointer.leftButton.isDown){
					this.game.paused = false;
					this.game.state.start('menu');
				}
			}
		else {
			this.back.alpha = 0.8;
			this.recBack.alpha = 0.5;
		}
	},

	update(){
		if(this.game.mode === 0) this.changeState();
		this.movePlane();
		if(pointerOutsideWorld(this.pointer)){
			this.airplane.animations.play('straight');
		}
		else if(this.pointer.x > this.airplane.x + 10){
			this.airplane.animations.play('right');
		}
		else if(this.pointer.x < this.airplane.x - 10){
			this.airplane.animations.play('left');
		}
		else {
			this.airplane.animations.play('straight');
		}

		//airplane hit by bullet
		for(var i =0; i < this.bullets.length; i++){
			var thisBullet = this.bullets.getChildAt(i);
			this.game.physics.arcade.overlap(thisBullet, this.airplane, ()=>{
				thisBullet.kill();
				this.airplaneHealth -= 10;
				this.game.sound.volume = this.a;
				this.crashSound.volume = this.game.sound.volume;
				this.crashSound.play();
				var tmp = 0;
				var interval = setInterval(()=>{
					this.healthbar.width -= 1;
					tmp += 1;
					if(tmp === 60){
						clearInterval(interval);
					}
				}, 10);
				this.bulletExplode.setYSpeed(-50, 50);
				this.bulletExplode.setXSpeed(-50, 50);
				this.bulletExplode.x = thisBullet.x;
				this.bulletExplode.y = thisBullet.y;
				this.bulletExplode.start(true, 800, null, 5);
				if(this.airplaneHealth > 0){
					this.emitter.setYSpeed(this.airplane.body.velocity.y - 50, this.airplane.body.velocity.y + 50);
					this.emitter.setXSpeed(this.airplane.body.velocity.x - 50, this.airplane.body.velocity.x + 50);
					this.emitter.x = this.airplane.x;
					this.emitter.y = this.airplane.y;
					this.emitter.start(true, 999, null, 15); 
				}
				else {
					this.explode.setYSpeed(this.airplane.body.velocity.y - 50, this.airplane.body.velocity.y + 50);
					this.explode.setXSpeed(this.airplane.body.velocity.x - 50, this.airplane.body.velocity.x + 50);
					this.explode.x = this.airplane.x;
					this.explode.y = this.airplane.y;
					this.explode.start(true, 3000, null, 30); 
					this.airplane.kill();
					this.game.score = this.score;
					this.game.time.events.add(3500, ()=>{
					this.game.leaderboard = [];
						var data = firebase.database().ref();
						if(this.game.mode === 1){
							data.push(parseInt(this.game.score));
						}
						data.once('value').then((snapshot)=>{
							for(var i in snapshot.val()){
								this.game.leaderboard.push(snapshot.val()[i]);
							}
							this.game.leaderboard = this.game.leaderboard.sort((a, b) => b-a);
							this.game.state.start('game-over');
						})
					}, this);
					
				}
			}, null, this);
		}

		//airplane gain hp
		for(var i =0; i<this.heals.length; i++){
			var thisHeal = this.heals.getChildAt(i);
			this.game.physics.arcade.collide(thisHeal, this.airplane, ()=>{
				if(this.airplaneHealth < 100){
					this.airplaneHealth += 5;
					var tmp = 0;
					var interval = setInterval(()=>{
						this.healthbar.width += 1;
						tmp += 1;
						if(tmp === 30){
							clearInterval(interval);
						}
					}, 10);
				}
				this.game.sound.volume = this.a;
				this.healSound.volume = this.game.sound.volume;
				this.healSound.play();
				thisHeal.kill();
			}, null, this)
		}

		//enemy wandering around
		for(var i=0; i< this.enemies.length; i++){
			var thisEnemy = this.enemies.getChildAt(i);
			if(Math.random() < 0.005 && thisEnemy.hitPoint > 0){
				this.createBullet(thisEnemy.x, thisEnemy.y, thisEnemy.body.velocity.x);
			}
			if(thisEnemy.y >= 100){
				if(thisEnemy.body.velocity.y !== 0){
					thisEnemy.body.velocity.y = 0;
					thisEnemy.body.velocity.x = 70 * this.game.rnd.pick([-1, 1]);
				}
				else{
					thisEnemy.body.velocity.x = Math.random() < 0.01? (-thisEnemy.body.velocity.x) : (thisEnemy.body.velocity.x);
				}

				if(thisEnemy.x <= 30){
					thisEnemy.body.velocity.x = 70;
				}
				if(thisEnemy.x >= 770){
					thisEnemy.body.velocity.x = -70;
				}
			}

			if(thisEnemy.body.velocity.x > 0){
				thisEnemy.scale.x = 1;
			}
			else {
				thisEnemy.scale.x = -1;
			}
		}

		//enemy and airplane bullet collide
		for(var i =0; i < this.fires.length; i++){
			var thisFire = this.fires.getChildAt(i);
			for(var j = 0; j < this.enemies.length; j++){
				var thisEnemy = this.enemies.getChildAt(j);
				
				if(thisEnemy.hitPoint < 0) continue;
				this.game.physics.arcade.overlap(thisFire, thisEnemy, ()=>{
					thisEnemy.hitPoint -= 10;
					
					if(thisEnemy.hitPoint === 0){
						this.score += 5;
						if(this.game.mode === 1) this.scoretxt.text = 'Score: ' + this.score;
						var dieAnim = thisEnemy.animations.play('die');
						dieAnim.killOnComplete = true;
						if(this.machinegunbar.height > -90){
							this.machinegunbar.height -= 5;
						}
						else{
							this.machinegunbar.height = -90;
						}

						//healing
						if(Math.random() > 0.8){
							this.createHeal(thisEnemy.x, thisEnemy.y + 40);
						}
					}
					thisFire.kill();
					if(thisEnemy.hitPoint >= 0){
						this.bulletExplode.setYSpeed(-50, 50);
						this.bulletExplode.setXSpeed(-50, 50);
						this.bulletExplode.x = thisFire.x;
						this.bulletExplode.y = thisFire.y - 50;
						this.bulletExplode.start(true, 800, null, 5);
					}
				}, null, this);
			}
		}

		//turbo
		if(this.turbobar.height > -90 && !this.pointer.leftButton.isDown){
			this.turbobar.height -= 0.1;
			this.turboing = false;
		}
		else if(this.turbobar.height <= -90 && !this.pointer.leftButton.isDown){
			this.turbobar.height = -90;
			this.turboing = false;
		}
		else if(this.turbobar.height < 0 && this.pointer.leftButton.isDown){
			this.turbobar.height += 0.5;
			this.turboing = true;
		}
		else {
			this.turbobar.height = 0;
			this.turboing = false;
		}
		//machine gun
		this.game.input.onTap.add((pointer, doubleTap)=>{
			if(doubleTap && this.machinegunbar.height <= -90){
				this.airplaneFireLoop.delay = 100;
			}
		});
		if(this.airplaneFireLoop.delay === 100 && this.machinegunbar.height < 0){
			this.machinegunbar.height += 0.2;
		}
		else {
			this.airplaneFireLoop.delay = 400;
		}
	},

	movePlane(){
		this.bg.tilePosition.y += 1;
		if(pointerOutsideWorld(this.pointer)){
			this.airplane.body.velocity.x = 0;
			this.airplane.body.velocity.y = 0;
			return;
		}
		if(!this.turboing){
			if(this.pointer.x > this.airplane.x + 10){
				this.airplane.body.velocity.x = (this.airplane.body.velocity.y !== 0)? 200 : 282;
				this.airplane.animations.play('right');
				this.bg.tilePosition.x += 0.1;
			}
			else if(this.pointer.x < this.airplane.x - 10){
				this.airplane.body.velocity.x = (this.airplane.body.velocity.y !== 0)? -200 : -282;
				this.airplane.animations.play('left');
				this.bg.tilePosition.x -= 0.1;
			}
			else {
				this.airplane.body.velocity.x = 0;
			}
	
			if(this.pointer.y > this.airplane.y + 10){
				this.airplane.body.velocity.y = (this.airplane.body.velocity.x !== 0) ? 200 : 282;
			}
			else if(this.pointer.y < this.airplane.y - 10){
				this.airplane.body.velocity.y = (this.airplane.body.velocity.x !== 0) ? -200 : -282;
			}
			else {
				this.airplane.body.velocity.y = 0;
			}
		}
		else {
			if(this.pointer.x > this.airplane.x + 10){
				this.airplane.body.velocity.x = (this.airplane.body.velocity.y !== 0)? 200 * 2 : 282 * 2;
				this.airplane.animations.play('right');
				this.bg.tilePosition.x += 0.1;
			}
			else if(this.pointer.x < this.airplane.x - 10){
				this.airplane.body.velocity.x = (this.airplane.body.velocity.y !== 0)? -200 * 2 : -282 * 2;
				this.airplane.animations.play('left');
				this.bg.tilePosition.x -= 0.1;
			}
			else {
				this.airplane.body.velocity.x = 0;
			}
	
			if(this.pointer.y > this.airplane.y + 10){
				this.airplane.body.velocity.y = (this.airplane.body.velocity.x !== 0) ? 200 * 2 : 282 * 2;
			}
			else if(this.pointer.y < this.airplane.y - 10){
				this.airplane.body.velocity.y = (this.airplane.body.velocity.x !== 0) ? -200 * 2 : -282 * 2;
			}
			else {
				this.airplane.body.velocity.y = 0;
			}
		}
	},

	createFire(){//airplane's bullet
		if(this.airplaneHealth <= 0) return;
		this.fire = this.game.add.sprite(this.airplane.x, this.airplane.y - 20, 'bullet', 0, this.fires);
		this.fire.anchor.setTo(0.5, 0.5);
		this.fire.body.velocity.y = -500;
		this.fire.checkWorldBunds = true;
		this.fire.outOfBoundsKill = true;
	},

	changeState(){
		if(this.score === 200 && this.stage === 1){
			this.stage = 0;
			var tmpInterval = setInterval(() => {
				this.stage = 2;
				clearInterval(tmpInterval);
			}, 10000);
		}
		else if(this.score === 400 && this.stage === 2){
			this.stage = 0;
			var tmpInterval = setInterval(() => {
				this.stage = 3;
				clearInterval(tmpInterval);
			}, 10000);
		}
		else if(this.score === 600 && this.stage ===3){
			this.stage = 0;
			var tmpInterval = setInterval(() => {
				this.stage = 4;
				var finish = true;
				for(var i = 0; i < this.enemies.length; i++){
					if(this.enemies.getChildAt(i).hitPoint > 0){
						finish = false;
					}
				}
				if(finish){
					clearInterval(tmpInterval);
					this.game.gameovertxt = 'YOU WON!';
					this.game.state.start('game-over');
				}
				
			}, 4000);
		}
	},

	createEnemy(){
		if(this.stage !== 1) return;
		// var enemy = this.game.add.sprite(0, 0, 'enemy', 0, this.enemies);
		var enemy = this.enemies.getFirstDead();
		if (!enemy) {
			return;
		}
		enemy.anchor.setTo(0.5, 1);
		enemy.reset(Math.random() * this.game.width, 0);
		enemy.anchor.setTo(0.5, 0.5);
		
		var flag = Math.random();
		if(flag < 0.07){
			enemy.body.velocity.y = 50;
			enemy.scale.setTo(1, 4);
			enemy.hitPoint = 200;
		}
		else if(flag < 0.2){
			enemy.body.velocity.y = 50;
			enemy.scale.setTo(2, 2);
			enemy.hitPoint = 50;
		}
		else {
			enemy.body.velocity.y = 50;
			enemy.scale.setTo(1, 1);
			enemy.hitPoint = 10;
		}
		

		enemy.animations.add('move', [0, 1, 2, 3, 4], 15,  true);
		enemy.animations.add('attack', [8, 9, 10, 11, 10, 9, 10, 11, 12], 10,  false);
		enemy.animations.add('hurt', [17, 18, 17], 10, false);
		enemy.animations.add('die', [16, 17, 18, 19, 20, 21, 22, 23], 30,  false);
		enemy.animations.play('move');
	},

	createEnemy2(){
		if(this.stage !== 2) return;
		var enemy = this.game.add.sprite(0, 0, 'enemy', 0, this.enemies);
		if (!enemy) {
			return;
		}
		enemy.anchor.setTo(0.5, 1);
		enemy.reset(Math.random() * this.game.width, 0);
		enemy.anchor.setTo(0.5, 0.5);
		
		var flag = Math.random();
		if(flag < 0.07){
			enemy.body.velocity.y = 50;
			enemy.scale.setTo(1, 4);
			enemy.hitPoint = 200;
		}
		else if(flag < 0.5){
			enemy.body.velocity.y = 50;
			enemy.scale.setTo(2, 2);
			enemy.hitPoint = 50;
		}
		else {
			enemy.body.velocity.y = 50;
			enemy.scale.setTo(1, 1);
			enemy.hitPoint = 10;
		}
		

		enemy.animations.add('move', [0, 1, 2, 3, 4], 15,  true);
		enemy.animations.add('attack', [8, 9, 10, 11, 10, 9, 10, 11, 12], 10,  false);
		enemy.animations.add('hurt', [17, 18, 17], 10, false);
		enemy.animations.add('die', [16, 17, 18, 19, 20, 21, 22, 23], 30,  false);
		enemy.animations.play('move');
	},

	createEnemy3(){
		if(this.stage !== 3) return;
		var enemy = this.game.add.sprite(0, 0, 'enemy', 0, this.enemies);
		if (!enemy) {
			return;
		}
		enemy.anchor.setTo(0.5, 1);
		enemy.reset(Math.random() * this.game.width, 0);
		enemy.anchor.setTo(0.5, 0.5);
		
		var flag = Math.random();
		if(flag < 0.2){
			enemy.body.velocity.y = 50;
			enemy.scale.setTo(1, 4);
			enemy.hitPoint = 200;
		}
		else if(flag < 0.5){
			enemy.body.velocity.y = 50;
			enemy.scale.setTo(2, 2);
			enemy.hitPoint = 50;
		}
		else {
			enemy.body.velocity.y = 50;
			enemy.scale.setTo(1, 1);
			enemy.hitPoint = 10;
		}
		

		enemy.animations.add('move', [0, 1, 2, 3, 4], 15,  true);
		enemy.animations.add('attack', [8, 9, 10, 11, 10, 9, 10, 11, 12], 10,  false);
		enemy.animations.add('hurt', [17, 18, 17], 10, false);
		enemy.animations.add('die', [16, 17, 18, 19, 20, 21, 22, 23], 30,  false);
		enemy.animations.play('move');
	},

	createBullet(x, y, v){
		this.bullet = this.game.add.sprite(x, y, 'fire-bullet', 0, this.bullets);
		this.bullet.anchor.setTo(0.5, 0.5);
		this.bullet.scale.setTo(0.9, 0.9);
		this.bullet.animations.add('fall', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59], 100, true);
		this.bullet.body.gravity.y = 300;
		this.bullet.body.velocity.x = v;
		this.bullet.checkWorldBunds = true;
		this.bullet.outOfBoundsKill = true;
		this.bullet.animations.play('fall');
	},

	createHeal(x, y){
		var heal = this.game.add.sprite(x, y, 'health', 0, this.heals);
		heal.anchor.setTo(0.5, 0.5);
		heal.scale.setTo(0.5, 0.5);
		heal.animations.add('fall', [0, 1, 2, 3, 4], 10, true);
		heal.body.gravity.y = 200;
		heal.checkWorldBunds = true;
		heal.outOfBoundsKill = true;
		heal.animations.play('fall');
	}
}

function pointerOutsideWorld(pointer){
	if(pointer.x > 800 || pointer.x < 0)
		if(pointer.y > 700 || pointer.y < 0)
			return true;

	return false;
}