window.onload = ()=>{
	var game = new Phaser.Game(800, 700, Phaser.AUTO, 'game-div');
	game.state.add('menu', Menu);
	game.state.add('load', Load);
	game.state.add('main', Main);
	game.state.add('game-over', GameOver);
	game.state.add('setting', Setting);
	game.state.start('load');
}
