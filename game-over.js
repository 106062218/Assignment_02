var GameOver = {
	preload(){
		if(this.game.mode === 1){
			this.game.add.text(this.game.width/2, 250, '1st: ' + this.game.leaderboard[0], {
				font: '20px Arial',
				fill: '#ffffff'
			}).anchor.setTo(0.5, 0.5);
			this.game.add.text(this.game.width/2, 290, '2nd: ' + this.game.leaderboard[1], {
				font: '20px Arial',
				fill: '#ffffff'
			}).anchor.setTo(0.5, 0.5);
			this.game.add.text(this.game.width/2, 330, '3rd: ' + this.game.leaderboard[2], {
				font: '20px Arial',
				fill: '#ffffff'
			}).anchor.setTo(0.5, 0.5);
			this.game.add.text(this.game.width/2, 400, 'Your score: ' + this.game.score, {
				font: '20px Arial',
				fill: '#ffffff'
			}).anchor.setTo(0.5, 0.5);
		}
	},

	create(){
		var gameOver = this.game.add.text(this.game.width/2, 150, this.game.gameovertxt, {
			font: '30px Arial',
			fill: '#ffffff'
		});
		gameOver.anchor.setTo(0.5, 0.5);

		this.backToMenu = this.game.add.text(this.game.width/2, 500, 'MENU', {
			font: '20px Arial',
			fill: '#ffffff'
		})
		this.backToMenu.anchor.setTo(0.5, 0.5);

		this.recStart = this.game.add.graphics(0, 0);
		this.recStart.lineStyle(2, 0x555555, 5);
		this.recStart.drawRect(this.backToMenu.x - 130, 500 - 25, 260, 45);

		this.cursor = this.game.input.keyboard.createCursorKeys();

		this.backToMenu.alpha = 1;
		this.pointer = this.game.input.mousePointer;
	},

	update(){
		if(this.pointer.x >= this.backToMenu.x - 130
			&& this.pointer.x <=this.backToMenu.x +130
			&& this.pointer.y >= 500 - 25
			&& this.pointer.y <= 500 + 25)
		{
			this.backToMenu.alpha = 1;
			this.recStart.alpha = 1;
	
			if(this.pointer.leftButton.isDown){
				this.game.state.start('menu');
			}
		}
		else{
			this.backToMenu.alpha = 0.8;
			this.recStart.alpha = 0.5;
		}
	}
}